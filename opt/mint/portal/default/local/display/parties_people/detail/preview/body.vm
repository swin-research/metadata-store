#* 
************************************************************************************************
  Directory: /opt/mint/portal/default/local/display/parties_people/detail/preview/
  Original code written by Queensland Cyber Infrastructure GFoundation (QCIF)
  Current Version: v1.6 Released: 2nd April 2013
  ReDBox-Mint repository: github.com/redbox-mint
 
  Modified by Arna Karick for Swinburne University of Technology - update July 2013

  This is the formatting recipe for 'Parties_People' records;
  - this directory overrides /opt/mint/portal/default/default & /opt/mint/portal/default/mint
  - modified to allow manual re-ordering of fields 
  - technical interna; Mint metadata appears last
  - additional fields can be added - for the list of available fields uncomment the last 7 lines
*************************************************************************************************
*#

#macro( displayValue $value )
  #if($value.size() > 1)
    #foreach($item in $value)
      <span class="meta-value">$self.escape($item)</span><br/>
    #end 
  #elseif($value.size() == 0)
       $self.escape("")
  #else
    $self.escape($value.get(0))
  #end
  #end
<table class="meta">

#*
Formatting recipe for Parties_People
*#
    
     <tr>
        <th width="25%">$parent.getFriendlyName("ID")</th>
        <td>#displayValue($metadata.getList("ID"))</td>
     </tr>
  
    <tr>
        <th width="25%">$parent.getFriendlyName("dc_title")</th>
        <td>#displayValue($metadata.getList("dc_title"))</td>
    </tr>
 
    <tr>
        <th width="25%">$parent.getFriendlyName("Honorific")</th>
        <td> #displayValue($metadata.getList("Honorific"))</td>
    </tr>
 
    <tr>
        <th width="25%">$parent.getFriendlyName("Given_Name")</th>
        <td> #displayValue($metadata.getList("Given_Name"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("Pref_Name")</th>
        <td>#displayValue($metadata.getList("Pref_Name"))</td>
    </tr>
 
     <tr>
        <th width="25%">$parent.getFriendlyName("Other_Names")</th>
        <td>#displayValue($metadata.getList("Other_Names"))</td>
     </tr>
  
     <tr>
        <th width="25%">$parent.getFriendlyName("Job_Title")</th>
        <td>#displayValue($metadata.getList("Job_Title"))</td>
     </tr>
 
     <tr>
        <th width="25%">$parent.getFriendlyName("dc_description")</th>
        <td>#displayValue($metadata.getList("dc_description"))</td>
     </tr>
 
     <tr>
        <th width="25%">$parent.getFriendlyName("Email")</th>
        <td>#displayValue($metadata.getList("Email"))</td>
     </tr>
 
     <tr>
        <th width="25%">$parent.getFriendlyName("STAFF_PROFILE")</th>
        <td>#displayValue($metadata.getList("STAFF_PROFILE"))</td>
    </tr>
 
    <tr>
        <th width="25%">$parent.getFriendlyName("PERSONAL_HOMEPAGE")</th>
        <td>#displayValue($metadata.getList("PERSONAL_HOMEPAGE"))</td>
    </tr>
 
    <tr>
        <th width="25%">$parent.getFriendlyName("NLA_Party_Identifier")</th>
        <td>#displayValue($metadata.getList("NLA_Party_Identifier"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("ready_for_nla")</th>
        <td>#displayValue($metadata.getList("ready_for_nla"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("ORCID_ID")</th>
        <td>#displayValue($metadata.getList("ORCID_ID"))</td>
    </tr>
 
    <tr>
        <th width="25%">$parent.getFriendlyName("Groups")</th>
        <td>#displayValue($metadata.getList("Groups"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("primary_group_id")</th>
        <td>#displayValue($metadata.getList("primary_group_id"))</td>
    </tr>
 
    <tr>
        <th width="25%">$parent.getFriendlyName("COUNTRY")</th>
        <td>#displayValue($metadata.getList("COUNTRY"))</td>
    </tr>

     <tr>
        <th width="25%">$parent.getFriendlyName("EXTORG")</th>
        <td>#displayValue($metadata.getList("EXTORG"))</td>
    </tr>

     <tr>
        <th width="25%">$parent.getFriendlyName("ANZSRC_FOR_CODES")</th>
        <td>#displayValue($metadata.getList("ANZSRC_FOR"))</td>
    </tr>

#*
Mint internal technical metadata
*#

    <tr>
        <th width="25%">$parent.getFriendlyName("repository_name")</th>
        <td>#displayValue($metadata.getList("repository_name"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("repository_type")</th>
        <td>#displayValue($metadata.getList("repository_type"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("oai_set")</th>
        <td>#displayValue($metadata.getList("oai_set"))</td>
    </tr>

   <tr>
        <th width="25%">$parent.getFriendlyName("oai_identifier")</th>
        <td>#displayValue($metadata.getList("oai_identifier"))</td>
   </tr>

   <tr>
        <th width="25%">$parent.getFriendlyName("known_ids")</th>
        <td>
            #set($valueList = $metadata.getList("known_ids"))
            #if($valueList.size() > 1)
                #foreach($value in $valueList)
                    <span class="meta-value">$self.escape($value)</Span><br/>
                #end
            #else
                $self.escape($valueList.get(0))
            #end
        </td>
    </tr>
 
    <tr>
        <th width="25%">$parent.getFriendlyName("dc_identifier")</th>
        <td>
            #set($valueList = $metadata.getList("dc_identifier"))
            #if($valueList.size() > 1)
                #foreach($value in $valueList)
                    <span class="meta-value">$self.escape($value)</span><br/>
                #end
            #else
                $self.escape($valueList.get(0))
            #end
        </td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("id")</th>
        <td>#displayValue($metadata.getList("id"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("storage_id")</th>
        <td>#displayValue($metadata.getList("storage_id"))</td>
    </tr>
    
    <tr>
        <th width="25%">$parent.getFriendlyName("dc_format")</th>
        <td>#displayValue($metadata.getList("dc_format"))</td>
    </tr>

    <tr>
    <th width="25%">$parent.getFriendlyName("harvest_rules")</th>
        <td>
            #set($valueList = $metadata.getList("harvest_rules"))
            #if($valueList.size() > 1)
                #foreach($value in $valueList)
                    <span class="meta-value">$self.escape($value)</span><br/>
                #end
            #else
                $self.escape($valueList.get(0))
            #end
        </td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("harvest_config")</th>
        <td> #displayValue($metadata.getList("harvest_config"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("last_modified")</th>
        <td>#displayValue($metadata.getList("last_modified"))</td>
    </tr>

     <tr>
        <th width="25%">$parent.getFriendlyName("pidProperty")</th>
        <td>#displayValue($metadata.getList("pidProperty"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("security_filter")</th>
        <td>#displayValue($metadata.getList("security_filter"))</td>
    </tr>

    <tr>
        <th width="25%">$parent.getFriendlyName("display_type")</th>
        <td>#displayValue($metadata.getList("display_type"))</td>
    </tr>

</table>

#*
To see  a list of available fields to display - uncomment the following 
*#

#*
<hr/>
#foreach($key in $metadata.getJsonObject().keySet())
  $key - 
  #displayValue($metadata.getList($key))
  <br/>
#end
<hr/>
*#