# Swinburne Metadata Stores Project

This is the source code repository for the ANDS funded Swinburne Metadata Store developed by Swinburne Research Information Services in partnership with the Victorian eResearch Strategic Initiative (VeRSI). 

This repository contains various harvest configuration files, modified to allow the inclusion of additional fields, and user interface files that were customised to suit the Swinburne deployment of [ReDBox and Mint](https://github.com/redbox-mint). 

For more information, please see [our website](http://www.research.swinburne.edu.au/our-research/ANDS/) and the [repository wiki](https://bitbucket.org/swin-research/metadata-store/wiki/Home)

User and Administrator guides are available on [our website](http://www.research.swinburne.edu.au/our-research/ANDS/connectToRedBox.html)

![Creative Commons License](http://i.creativecommons.org/l/by-nc-sa/3.0/au/88x31.png "Creative Commons License") 
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Australia License](http://creativecommons.org/licenses/by-nc-sa/3.0/au/deed.en_US")