<?php
   
	
/* Utility to process redbox csv export and load to database   */
               
/* Author:  Neale Yates (Recast Data) for  Swinburne Research July 2013  */

if (1) //$pword ==1)
{
   
    /* include file with login credentials for RM and mysql database- not included in repository*/
	include 'db_credentials.php';
	
	//assume fresh start with each load
	$sql = "delete from redbox";
	$result = mysql_query($sql);
	
	$sql = "delete from rb_forcodes";
	$result = mysql_query($sql);
	
	$sql = "delete from rb_seocodes";
	$result = mysql_query($sql);
	
	$sql = "delete from rb_keywords";
	$result = mysql_query($sql);
	
	
	
        echo "Redbox data import <br />";
     
  				 
	$filename = 'redbox_export.csv';
	$handle = fopen($filename, "r");
	$contents = fread($handle, filesize($filename));
	fclose($handle);
	
	//echo $contents;
	
	$str_out = "";
			
	$rows = explode("\n", $contents);  // split file into rows using end of line
	
	echo $count = count($rows);
	$first_row = 1;
	$a_col = -1;
	
	
	
	foreach($rows as $record)   //process each row
	{
		$fields = str_getcsv($record, ",", '"');  //split row into fields using tab delimiter
		//print_r($fields);
		
		if($fields[0] != 'ReDBox ID' && strlen($fields[0]) > 0)
		{
			
			
			for($ii = 0; $ii < 40; $ii++)
			{
				if($ii == 18 && isset($fields[$ii]))  //forcodes
				{
					//echo $fields[18];
					$items = explode(";", $fields[$ii]);
					//print_r($items);
					foreach($items as $item)
					{
						$forcode = strtok($item, "-");
						$l = strlen($forcode);
						$forcode = substr($forcode, 0, $l -1);
						$sql = "insert into rb_forcodes (rbid, forcode) values ('$fields[0]', '$forcode')";
						$result = mysql_query($sql);
					}
				}
				
				if($ii == 19 && isset($fields[$ii])) //seocodes
				{
					//echo $fields[18];
					$items = explode(";", $fields[$ii]);
					//print_r($items);
					foreach($items as $item)
					{
						$forcode = strtok($item, "-");
						$l = strlen($forcode);
						$forcode = substr($forcode, 0, $l -1);
						$sql = "insert into rb_seocodes (rbid, seocode) values ('$fields[0]', '$forcode')";
						$result = mysql_query($sql);
					}
				}
				
				if($ii == 20 && isset($fields[$ii]))  //keywords
				{
					//echo $fields[18];
					$items = explode(";", $fields[$ii]);
					//print_r($items);
					foreach($items as $item)
					{
						
						$sql = "insert into rb_keywords (rbid, keyword) values ('$fields[0]', '$item')";
						$result = mysql_query($sql);
					}
				}
			}
			
			$sql = "INSERT INTO `redbox`(`redboxid`, `title`, `record_type`, `workflow_step`, `date_created`, `date_modified`, `language`, `date_coverage`, `time_period`, `geospatial_location`, `description`, `related_pub`, `related_website`, `related_data`, `creators`, `contact`, `supervisor`, `collaborators`, `forcode`, `seocode`, `keywords`, `typeofactivity`, `access_rights`, `license`, `location_url`, `location`, `retention_period`, `extent`, `disposal_date`, `data_owner`, `data_custodian`, `data_affiliation`, `funding_body`, `grant_ids`, `project_title`, `depositor`, `data_size`, `IDM_policy`, `dmp`, `notes`)
					values
					('$fields[0]', '$fields[1]', '$fields[2]', '$fields[3]', '$fields[4]', '$fields[5]', '$fields[6]', '$fields[7]', '$fields[8]', '$fields[9]', '$fields[10]', '$fields[11]', '$fields[12]', '$fields[13]', '$fields[14]', '$fields[15]', '$fields[16]', '$fields[17]', '$fields[18]', '$fields[19]', '$fields[20]', '$fields[21]', '$fields[22]', '$fields[23]', '$fields[24]', '$fields[25]', '$fields[26]', '$fields[27]', '$fields[28]', '$fields[29]', '$fields[30]', '$fields[31]', '$fields[32]', '$fields[33]', '$fields[34]', '$fields[35]', '$fields[36]', '$fields[37]', '$fields[38]', '$fields[39]')";
			$result = mysql_query($sql);
		}
		
					
	}
		
		
		
		
}


  ?>
