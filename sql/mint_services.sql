-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2013 at 03:35 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `swin_researcher`
--

-- --------------------------------------------------------

--
-- Table structure for table `mint_services`
--

CREATE TABLE IF NOT EXISTS `mint_services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(20) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `service_type` varchar(255) NOT NULL,
  `ANZSRC_FOR_1` varchar(10) NOT NULL,
  `ANZSRC_FOR_2` varchar(10) NOT NULL,
  `ANZSRC_FOR_3` varchar(10) NOT NULL,
  `Location` varchar(255) NOT NULL,
  `Coverage_Temporal_From` date NOT NULL,
  `Coverage_Temporal_To` date NOT NULL,
  `Coverage_Spatial_Type` varchar(255) NOT NULL,
  `Coverage_Spatial_Value` varchar(255) NOT NULL,
  `Existence_Start` date NOT NULL,
  `Existence_End` date NOT NULL,
  `Website` varchar(255) NOT NULL,
  `Data_Quality_Information` varchar(255) NOT NULL,
  `Reuse_Information` varchar(255) NOT NULL,
  `Access_Policy` varchar(255) NOT NULL,
  `URI` varchar(255) NOT NULL,
  `Description` longtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
