-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2013 at 03:35 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `swin_researcher`
--

-- --------------------------------------------------------

--
-- Table structure for table `mint_people_extra`
--

CREATE TABLE IF NOT EXISTS `mint_people_extra` (
  `rm_cid` varchar(10) NOT NULL DEFAULT '',
  `nla_id` varchar(100) NOT NULL,
  `orchid_id` varchar(100) NOT NULL,
  `staff_profile` varchar(100) NOT NULL,
  `staffid` varchar(10) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `pers_homepage` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `pref_name` varchar(255) NOT NULL,
  PRIMARY KEY (`rm_cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
