<html>
<head>
    <title>ReDLine</title>
    <!-- Load jQuery -->
    <script language="javascript" type="text/javascript" 
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js">
    </script>
    <!-- Load Google JSAPI -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", { packages: ["corechart"] });
        google.setOnLoadCallback(drawChart);
	google.setOnLoadCallback(drawChart2);
	google.setOnLoadCallback(drawChart3);
	google.setOnLoadCallback(drawChart4);
	google.setOnLoadCallback(drawChart5);
	
	
      function drawChart3() {
      
	 var jsonData = $.ajax({
                url: "http://localhost/mint/private/ajx_progress.php",
                dataType: "json",
                async: false
            }).responseText;

     
	var obj = jQuery.parseJSON(jsonData);
	    
            var data = google.visualization.arrayToDataTable(obj);
	    
        var options = {
          title: 'Progress',
          hAxis: {title: 'Month', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div3'));
        chart.draw(data, options);
      }


        function drawChart() {
            var jsonData = $.ajax({
                url: "http://localhost/mint/private/ajx_type.php",
                dataType: "json",
                async: false
            }).responseText;

     
	var obj = jQuery.parseJSON(jsonData);
	    
            var data = google.visualization.arrayToDataTable(obj);

            var options = {
                title: 'Record Types'
            };

            var chart = new google.visualization.PieChart(
                        document.getElementById('chart_div'));
            chart.draw(data, options);
        }
	
	
        function drawChart2() {
            var jsonData = $.ajax({
                url: "http://localhost/mint/private/ajx_stage.php",
		<!--   send GET variable to url: data: { location: "Doughnut" }, -->
                dataType: "json",
                async: false
            }).responseText;

     
	var obj = jQuery.parseJSON(jsonData);
	    
            var data = google.visualization.arrayToDataTable(obj);

            var options = {
                title: 'Curation Stage'
            };

            var chart = new google.visualization.PieChart(
                        document.getElementById('chart_div2'));
            chart.draw(data, options);
        }
	
	 function drawChart4() {
            var jsonData = $.ajax({
                url: "http://localhost/mint/private/ajx_for.php",
		<!--   send GET variable to url: data: { location: "Doughnut" }, -->
                dataType: "json",
                async: false
            }).responseText;

     
	var obj = jQuery.parseJSON(jsonData);
	    
            var data = google.visualization.arrayToDataTable(obj);

            var options = {
                title: 'FOR code'
            };

            var chart = new google.visualization.PieChart(
                        document.getElementById('chart_div4'));
            chart.draw(data, options);
        }
	
	function drawChart5() {
            var jsonData = $.ajax({
                url: "http://localhost/mint/private/ajx_seo.php",
		<!--   send GET variable to url: data: { location: "Doughnut" }, -->
                dataType: "json",
                async: false
            }).responseText;

     
	var obj = jQuery.parseJSON(jsonData);
	    
            var data = google.visualization.arrayToDataTable(obj);

            var options = {
                title: 'SEO code'
            };

            var chart = new google.visualization.PieChart(
                        document.getElementById('chart_div5'));
            chart.draw(data, options);
        }

    </script>
</head>
<body>
    <section>
      <h3>Redbox Activity and Coverage</h3>
      <table width="100%" cellpadding="5" cellspacing="5">
      	<tr>
      		<th width="33%">&nbsp;</th>
		<th width="34%">&nbsp;</th>
		<th width="33%">&nbsp;</th>
            
      	</tr>
      	<tr class="charts">
		<td><div id="chart_div" style="width: 100%; height: 250px;"></div></td>
		<td><div id="chart_div2" style="width: 100%; height: 250px;"></div></td>
		
            
        </tr>
	
	<tr class="charts">

		<td><div id="chart_div4" style="width: 100%; height: 250px;"></div></td>
		<td><div id="chart_div5" style="width: 100%; height: 250px;"></div></td>
            
        </tr>
	
	
	<tr class="charts">
		<td colspan = 2><div id="chart_div3" style="width: 100%; height: 250px;"></div></td>
		
            
        </tr>
      </table>
      
</section>

<!--  <div id="chart_div" style="width: 900px; height: 500px;">
    </div>
    <div id="chart_div2" style="width: 900px; height: 500px;">
    </div> -->
</body>
</html>