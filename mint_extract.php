<?php

/** *****************************************************************************************************
*  mint_extract.php
* author: Neale Yates, Recast Data for Swinburne Research March 7, 2013; updated June 2013
*
* This script is intended to connect to Research Master (RM) and extract data to be load into MINT.  csv files is containing the data are saved locally.
* In addition to the RM data additional fields are extracted from mysql tables (mint_people_extra, mint_group_extra, mint_services, library) to provide supplementary data not currently available in RM
*
*********************************************************************************************************/
   
if(!isset($_POST['transfer']) && $_POST['transfer'] != 'transfer')
{

	//include 'authenticate.php';
	//include 'access.php';

	 $dbname = 'ord';
	 
	$errors = 0;
	$file_count = 0;

	/* include file with login credentials for RM and mysql database- not included in repository*/
	include 'db_credentials.php';

	    if (!$conn) 
	    {
		$e = oci_error();
		trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
		echo "connect failed??";
	    }
	    else
	    {
		//echo "connected?";
	    }
		echo("<p><b>\"AOU\" - data is extracted from RM using lcurrent = 1. Supplementary data loaded from 'mint_group_extra' mysql table </b></p> ");

		$sql = "select aou.ccode, aou.caou_code, aou.saouname, man.ccode 
				from rm4xaou aou, rm4xaou man
				where aou.cmanage_code = man.caou_code
				and aou.lcurrent = 1
				order by aou.ccode";
				
		$stid = oci_parse($conn, $sql);
			    
		oci_execute($stid);
		echo oci_error();
		
		$str_out = 'ID,"Name","Email","Phone","Parent_Group_ID","URI","NLA_Party_Identifier","Homepage","Description"';  //variable to accumulate data to be written to csv file
		$str_out .= "\n";
		
		$str_out .= 'g0000000070,"Swinburne","","","","","",""';   //hard code top level Swinburne AOU
		$str_out .= "\n";
		    
		echo("<table>");        
		echo("<tr><th>ID<th>Name<th>Email<th>Phone<th>Parent_Group_ID<th>URI<th>NLA_Party_Identifier<th>Homepage<th>Description");
		while ($o_row = oci_fetch_array($stid, OCI_BOTH+OCI_RETURN_NULLS)) 
		 {
			
			$profile_flag = 0;
			$sqle = "select email, phone, uri, nla_party_id, homepage, description 
					from mint_group_extra
					where id = '$o_row[0]'";
			
			$resulte = mysql_query($sqle);
			$recorde = mysql_fetch_row($resulte);
			
			$rowse = mysql_num_rows($resulte);
			if($rowse == 0)
			{
				$sqle1 = "insert into mint_group_extra (id, description) values ('$o_row[0]', '$o_row[2]')";
				$resulte1 = mysql_query($sqle1);
			}
			
			
			
			echo("<tr><td>");        //ID
			echo htmlentities($o_row[0], ENT_QUOTES);
			$str_out .= 'g'.trim($o_row[0]).',';
					
			
			
			 echo("<td>");        //Name
			echo htmlentities($o_row[2], ENT_QUOTES);
			$str_out .= '"'.trim($o_row[2]).'",';
			
			echo("<td>");        //email
			echo $recorde[0];
			$str_out .= '"'.trim($recorde[0]).'",';
			
			echo("<td>");        //phone
			echo $recorde[1];
			$str_out .= '"'.trim($recorde[1]).'",';
			
			if(trim($o_row[3]) == trim($o_row[0]))
			{
				$cmanage = "g0000000070";
			}
			else
			{
				$cmanage = 'g'.$o_row[3];
			}
			
			echo("<td>");        //parent group
			echo $cmanage;
			$str_out .= '"'.trim($cmanage).'",';
			
			echo("<td>");        //URI
			echo $recorde[2];
			$str_out .= '"'.trim($recorde[2]).'",';
			
			
			echo("<td>");        //nla_party_id
			echo $recorde[3];
			$str_out .= '"'.trim($recorde[3]).'",';
			
			echo("<td>");        //homepage
			echo $recorde[4];
			$str_out .= '"'.trim($recorde[4]).'",';
			
			echo("<td>");        //description
			
			$description = $o_row[1]."  ".$recorde[5];
		
			$d = strtok($description, "\n\r");
			while($x =strtok( "\n\r"))
			{
				$d .= " ".$x;
			}
			
			echo $d;
			$str_out .= '"'.trim($d).'"';
					
			
			$str_out .= "\n";
			
		 }
		 echo("</table>");
		 
		 $outfile = fopen('Parties_Groups_Swin.csv', "w");
			
			
		if(fwrite($outfile, $str_out))
		{
			echo "<br /><br />AOU data written to file";
			$file_count++;
		}
		else
		{
			echo "<br /><br />write failed!";
			$errors = 1;
		}
		fclose($outfile);
		
		echo("<br /><br />+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br /><br />");
		
		echo("<p><b>\"Parties\" - data is extracted from RM based on presence on 'MINT' tag. Supplementary data loaded from 'mint_people_extra' mysql table </b></p> ");

	    /*In order to avoid issues arising from duplicate personnel entries in RM,, individuals to be extracted from RM have been previously identified and tagged in the rm4sper.sanalysis field with the string "MINT".  Only personel records with the MINT tag will be retrieved.*/
	    
		$sql = "select rm4xper.cperson as ID, rm4xper.sgivenname as Given_Name, rm4xper.smidname as Other_Names, rm4xper.ssurname as Family_Name, rm4xper.sprefgiven as Pref_Name, rm4xper.spertitle as Honorific, rm4xper.semail as Email, rm4xper.caou as GroupID_1, rm4xper.cperson, sorganis, rm4xper.scountry, rm4xper.sidnum, trim(rm4xper.cmanage_code), ctype, rm4xper.sanalysis
				from rm4xper left join rm4xorg on rm4xper.cextorg = rm4xorg.ccode
				where (rm4xper.sanalysis = 'MINT' or rm4xper.sanalysis = 'eMINT')";
				
				/*$sql = "select cperson as ID, sgivenname as Given_Name, smidname as Other_Names, ssurname as Family_Name, sprefgiven as Pref_Name, spertitle as Honorific, semail as Email, caou as GroupID_1, cperson, sextorg, scountry, sidnum, trim(cmanage_code)
				from rm4xper 
				where sanalysis = 'MINT' ";*/
			//	and (ctype = 'Internal' or ctype = 'Student' or ctype = 'Both')";

	    $stid = oci_parse($conn, $sql);
			    
	    oci_execute($stid);
	    echo oci_error();
	    
	    
	    
	    $str_out = '"ID","Given_Name","Other_Names","Family_Name","Pref_Name","Honorific","Email","Job_Title","Groups","EXTORG","COUNTRY","ANZSRC_FOR","NLA_Party_Identifier","ORCID_ID","STAFF_PROFILE","PERSONAL_HOMEPAGE","Description"';  //variable to accumulate data to be written to csv file
	    $str_out .= "\n";
		
	    echo("<table>");        
	    echo("<tr><th>ID<th>Given_Name<th>Other_Names<th>Family_Name<th>Pref_Name<th>Honorific<th>Email<th>Job_Title<th>Groups<th>EXTORG<th>COUNTRY<th>ANZSRC_FOR<th>NLA_Party_Identifier<th>ORCID_ID<th>STAFF_PROFILE<th>PERSONAL_HOMEPAGE<th>Description");
	    while ($o_row = oci_fetch_array($stid, OCI_BOTH+OCI_RETURN_NULLS)) 
	    {
		
		$profile_flag = 0;

		$sql3 = "select trim(ccode), saouname from rm4xaou where trim(caou_code) = '$o_row[12]'";
		 $stid3 = oci_parse($conn, $sql3);
			    
		oci_execute($stid3);
		echo oci_error();
		$o_row3 = oci_fetch_array($stid3, OCI_BOTH+OCI_RETURN_NULLS);
		
		
		$sqle = "select nla_id, orchid_id, staff_profile, job_title, pers_homepage, description, staffid from mint_people_extra where rm_cid = '$o_row[0]'";
		
		$resulte = mysql_query($sqle);
		$rowse = mysql_num_rows($resulte);
		if($rowse == 0)
		{
			$sqle1 = "insert into mint_people_extra (rm_cid, staffid) values ('$o_row[0]', '$o_row[11]')";
			$resulte1 = mysql_query($sqle1);
			
			
		}
		
		echo mysql_error();
		$recorde = mysql_fetch_row($resulte);
		
		//get library 'pid' to contruct staff profile page string
		$sql_l = "select pid from library where opax = '$recorde[6]'";
		$result_l = mysql_query($sql_l);
		$rows_l = mysql_num_rows($result_l);
		
		if($rows_l > 0)
		{
			$record_l = mysql_fetch_row($result_l);
			
			$profile_string = "http://www.swinburne.edu.au/swinburneresearchers/index.php?fuseaction=profile&pid=".$record_l[0];
			$profile_flag = 1;
		}
		
		echo("<tr><td>");        //ID
		echo htmlentities($o_row[0], ENT_QUOTES);
		$str_out .= 'p'.trim($o_row[0]).',';
		echo("<td>");             //Given Name
		echo htmlentities($o_row[1], ENT_QUOTES);
		$str_out .= '"'.trim($o_row[1]).'",';
		echo("<td>");            // Other Names
		echo htmlentities($o_row[2], ENT_QUOTES);     
		$str_out .= '"'.trim($o_row[2]).'",';
		echo("<td>");            // family names
		echo htmlentities($o_row[3], ENT_QUOTES);   
		$str_out .= '"'.trim($o_row[3]).'",';
		echo("<td>");            //Pref name
		echo htmlentities($o_row[4], ENT_QUOTES);  
		$str_out .= '"'.trim($o_row[4]).'",';
		echo("<td>");           //Honorific
		echo htmlentities($o_row[5], ENT_QUOTES);   
		$str_out .= '"'.trim($o_row[5]).'",';
		echo("<td>");          // email
		echo htmlentities($o_row[6], ENT_QUOTES);  
		$str_out .= '"'.trim($o_row[6]).'",';	
			
		echo("<td>$recorde[3]");  // job title
		$str_out .= '"'.$recorde[2].'",';
		
		if($o_row[7])
		{
			echo("<td>");        //groups
			echo htmlentities($o_row[7], ENT_QUOTES);    
			//echo $o_row3[0];
			$str_out .= '"'.'g'.trim($o_row[7]);
		}
		else
		{
			echo("<th>Missing Group data");  
			$errors = 1;
		}
		if($o_row3[0])   // if managing unit available append to group_id
		{
			$str_out .= ';';
			$str_out .= 'g'.$o_row3[0];
		}
		
		$str_out .= '",';   //terminate group string
		
		 /*extra dta for externals */
		
		echo("<td>");        //external organisation
		if(trim($o_row[14]) == 'eMINT')
		{
			echo htmlentities($o_row[9], ENT_QUOTES); 
			$str_out .= '"'.trim($o_row[9]).'",';
		}
		else
		{
			$str_out .= '"",';
		}
		
		echo("<td>");        //country
		echo htmlentities($o_row[10], ENT_QUOTES); 	
		$str_out .= '"'.trim($o_row[10]).'",';
		
		
	 /*	$sql2 = "select ecategory from 
					rm4scla, rm4scat 
					where rm4scla.cperson = '$o_row[8]' 
					and rm4scla.ccattype = '0000001054' 
					and rm4scla.ccode = rm4scat.ccode 
					order by npercent desc"; */
					
		$sql2 = "select ccode_code from 
					rm4xcla
					where rm4xcla.cperson = '$o_row[8]' 
					and ccategory = '0000008590'
					and lcurrent = 1
					order by npercent desc";
		$stid2 = oci_parse($conn, $sql2);
			    
		oci_execute($stid2);
		echo oci_error();
		
		$for = array();
		$i = 1;
		while ($o_row2 = oci_fetch_array($stid2, OCI_BOTH+OCI_RETURN_NULLS)) 
		{
			$for[$i] = htmlentities($o_row2[0], ENT_QUOTES); 
			$i++;
		}
		$str_out .= '"';
		if(isset($for[1]))
		{
			echo("<td> $for[1]");   //FOR codes
			$str_out .= trim($for[1]);
		}
		if(isset($for[2]))
		{
			echo(";$for[2]");
			$str_out .= ";".trim($for[2]);
		}
		if(isset($for[3]))
		{
			echo(";$for[3]");
			$str_out .= ";".trim($for[3]);
		}
		
		$str_out .= '",';    //terminate FOR field 
		
		//extra attributes for mint (ie non RM data
		
		echo("<td>$recorde[0]");
		$str_out .= '"'.$recorde[0].'",';	//nla identifier
		echo("<td>$recorde[1]");
		$str_out .= '"'.$recorde[1].'",';	// orchid ID
		
		if($profile_flag == 1)   //staff profile page
		{
			echo("<td>$profile_string");
			$str_out .= '"'.$profile_string.'",'; 
		}
		else
		{
			echo("<td>$recorde[2]");
			$str_out .= '"'.$recorde[2].'",';      
		}
		echo("<td>$recorde[4]");
		$str_out .= '"'.$recorde[4].'",';      //personal homepage
		echo("<td>$recorde[5]");
		 if(trim($o_row[14]) == 'eMINT')
		 {
			if($o_row[7])
			{
				$sql4 = "select saouname from rm4xaou where ccode = '$o_row[7]'";
				$stid4 = oci_parse($conn, $sql4);
				    
				oci_execute($stid4);
				echo oci_error();
				$o_row4 = oci_fetch_array($stid4, OCI_BOTH+OCI_RETURN_NULLS);
				
				$home = trim($o_row4[0]);
				
			}
			else
			{
				$home = trim($o_row3[1]);
			}
		
			 if(strlen($home) > 0)
			 {
				$desc = trim($o_row[4]). " ".trim($o_row[3])." was formerly a researcher with the $home, Swinburne University of Technology";
			}
			else
			{
				$desc = trim($o_row[4]). " ".trim($o_row[3])." was a researcher with the Swinburne University of Technology";
			}
			$description = $desc;
		
			$d = strtok($description, "\n\r");
			while($x =strtok( "\n\r"))
			{
				$d .= " ".$x;
			}
		
			$str_out .= '"'.trim($d).'"';      //description
		 }
		else if($recorde[5])
		{
			$description = $o_row[1];
		
			$d = strtok($description, "\n\r");
			while($x =strtok( "\n\r"))
			{
				$d .= " ".$x;
			}
			
			$str_out .= '"'.$d.'"';      //description
		}
		else
		{
			//construct default description string
			
			if($o_row[7])
			{
				$sql4 = "select saouname from rm4xaou where ccode = '$o_row[7]'";
				$stid4 = oci_parse($conn, $sql4);
				    
				oci_execute($stid4);
				echo oci_error();
				$o_row4 = oci_fetch_array($stid4, OCI_BOTH+OCI_RETURN_NULLS);
				
				$home = trim($o_row4[0]);
				
			}
			else
			{
				$home = trim($o_row3[1]);
			}
		
			 if(strlen($home) > 0)
			 {
				$desc = trim($o_row[4]). " ".trim($o_row[3])." is a researcher with the $home, Swinburne University of Technology";
			}
			else
			{
				$desc = trim($o_row[4]). " ".trim($o_row[3])." is a researcher with the Swinburne University of Technology";
			}
			
			$description = $desc;
		
			$d = strtok($description, "\n\r");
			while($x =strtok( "\n\r"))
			{
				$d .= " ".$x;
			}
		
			$str_out .= '"'.trim($d).'"';      //description
		}
		
		$str_out .= "\n";
	    }
	    echo("</table>");   

		//echo $str_out;
		
		$outfile = fopen('Parties_People_Swin.csv', "w");
			
			
		if(fwrite($outfile, $str_out))
		{
			echo "<br /><br />data written to file";
			$file_count++;
		}
		else
		{
			echo "<br /><br />write failed!";
			$errors = 1;
		}
		fclose($outfile);
		
	echo("<br /><br />+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br /><br />");
		
		echo("<p><b>\"Activities\" - data is extracted from RM based on presence on funded  = true. </b></p> ");
		
		
		
		$sql = "select distinct rm4spro.ccode, sprotitle, dstart, dend, mdesc, sfolio, soppabbrev, saouabbrev, rm4spro.caou, sgranttype
				from rm4spro, rm4soppl, rm4saou
				where lfunded = 1
				and rm4soppl.lprimary = 1
				and rm4spro.cproject =  rm4soppl.cproject
				and rm4saou.ccode = rm4spro.caou";
	      
	  /*   alternate sql which uses left join to extract a larger data set including projects with no recorded fund source or aou.  Decision made with AK not to use this as these 
		are incomplete records and should not be loaded to MINT .  
		Note there are additional projects excluded because tehre is no fund source marked as primary - also to be excluded
		
				$sql = " select rm4spro.ccode, sprotitle, dstart, dend, mdesc, sfolio, soppabbrev, saouabbrev, rm4spro.caou
				from rm4spro 
					left join rm4saou on rm4saou.ccode = rm4spro.caou
					left join rm4soppl on  rm4spro.cproject =  rm4soppl.cproject
				where lfunded = 1
				and rm4soppl.lprimary = 1";*/

	    $stid = oci_parse($conn, $sql);
			    
	    oci_execute($stid);
	    echo oci_error();
	    
	    $str_out = '"ID","Name","Type","Existence_start","Existence_end","Description","Primary_Investigator","Website","ANZSRC_FOR_1","ANZSRC_FOR_2","ANZSRC_FOR_3","External_code","Fund_Scheme","Group","Institution"';  //variable to accumulate data to be written to csv file
	    $str_out .= "\n";
		    
	    echo("<table>");        
	    while ($o_row = oci_fetch_array($stid, OCI_BOTH+OCI_RETURN_NULLS)) 
	    {
	      /*  $sqle = "select nla_id, orchid_id, staff_profile from mint_people_extra where staffid = '$o_row[0]'";
		
		$resulte = mysql_query($sqle);
		echo mysql_error();
		$recorde = mysql_fetch_row($resulte);*/
		
		echo("<tr><td>");        
		echo htmlentities($o_row[0], ENT_QUOTES);
		$str_out .= 'a'.trim($o_row[0]).',';
		echo("<td>");        
		
		$description = $o_row[1];
		//why???  maybe to remove line breaks
		$d = strtok($description, "\n\r");
		while($x =strtok( "\n\r"))
		{
			$d .= " ".$x;
		}
		/*echo htmlentities($d);  
		$str_out .= '"'.trim($d).'",';*/
		
		echo htmlentities($d, ENT_QUOTES);
		$str_out .= '"'.trim($d).'",';
		
		echo("<td>Project");   //assume 'Project type
		$str_out .= '"'."Project".'",';
		echo("<td>");        
		echo htmlentities($o_row[2], ENT_QUOTES);     
		$str_out .= '"'.trim($o_row[2]).'",';	
		echo("<td>");        
		echo htmlentities($o_row[3], ENT_QUOTES);   
		$str_out .= '"'.trim($o_row[3]).'",';
		echo("<td>");        
		
		
		$description = $o_row[4];
		
		$d = strtok($description, "\n\r");
		while($x =strtok( "\n\r"))
		{
			$d .= " ".$x;
		}
		echo htmlentities($d);  
		$str_out .= '"'.trim($d).'",';
		      
		
		//get PI
		
		$sql2 = "select cperson from rm4xperl where cproject = '$o_row[0]' and lprimary = 1";
		 $stid2 = oci_parse($conn, $sql2);
			    
		oci_execute($stid2);
		echo oci_error();
		
		$o_row2 = oci_fetch_array($stid2, OCI_BOTH+OCI_RETURN_NULLS);
		
		echo("<td>");  //principal investigator
		echo htmlentities($o_row2[0], ENT_QUOTES);  
		$str_out .= '"'.'p'.trim($o_row2[0]).'",';
		
		echo("<td>&nbsp;");    //place holder for website string
		$str_out .= '""'.",";
		
		/*$sql3 = "select ecategory from 
					rm4scla, rm4scat 
					where rm4scla.cpproject = '$o_row[0]' 
					and rm4scla.ccattype = '0000001054' 
					and rm4scla.ccode = rm4scat.ccode 
					order by npercent desc"; */
					
		$sql3 = "select ccode_code from 
					rm4xcla
					where rm4xcla.cproject = '$o_row[0]' 
					and ccategory = '0000008590'
					and lcurrent = 1
					order by npercent desc";
		$stid3 = oci_parse($conn, $sql3);
			    
		oci_execute($stid3);
		echo oci_error();
		
		$for = array();
		$i = 1;
		while ($o_row2 = oci_fetch_array($stid3, OCI_BOTH+OCI_RETURN_NULLS)) 
		{
			$for[$i] = htmlentities($o_row2[0], ENT_QUOTES); 
			$i++;
		}
		
		if(isset($for[1]))
		{
			echo("<td> $for[1]");
			$str_out .= '"'.trim($for[1]).'",';
		}
		else
		{
			echo("<td> &nbsp; ");
			$str_out .= '""'.",";
		}
		if(isset($for[2]))
		{
			echo("<td> $for[2]");
			$str_out .= '"'.trim($for[2]).'",';
		}
		else
		{
			echo("<td> &nbsp; ");
			$str_out .= '""'.",";
		}
		if(isset($for[3]))
		{
			echo("<td> $for[3]");
			$str_out .= '"'.trim($for[3]).'",';
		}
		else
		{
			echo("<td> &nbsp; ");
			$str_out .= '"",';
		}
		echo("<td>");  
		echo htmlentities($o_row[5], ENT_QUOTES);  
		$str_out .= '"'.trim($o_row[5]).'",';
		
		echo("<td>");  
		echo htmlentities($o_row[6], ENT_QUOTES);  
		$str_out .= '"'.trim($o_row[6]).'",';
		
		echo("<td>");  //group
		//echo htmlentities($o_row[7], ENT_QUOTES);  
		echo htmlentities($o_row[8], ENT_QUOTES);  
		$str_out .= '"'.'g'.trim($o_row[8]).'",';
		
		echo("<td>");
		if($o_row[9] != '0000017802')
		{
			echo "Swinburne University of Technology";  
			$str_out .= '"Swinburne University of Technology"';
		}
		else
		{
			echo "Other";  
			$str_out .= '""';
		}
		
		
		$str_out .= "\n"; 
	    }
	    echo("</table>");   

		//echo $str_out;
		
		$outfile = fopen('Activities_Swin.csv', "w");
			
			
		if(fwrite($outfile, $str_out))
		{
			echo "<br /><br />data written to file";
			$file_count++;
		}
		else
		{
			echo "<br /><br />write failed!";
			$errors = 1;
		}
		fclose($outfile);
		
		

		
	echo("<br /><br />+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br /><br />");
		
		echo("<p><b>\"Services\" - data is extracted from mysql table </b></p> ");
		
		
		 $sql = "select ID, service_id, Name, service_type, ANZSRC_FOR_1, ANZSRC_FOR_2, ANZSRC_FOR_3, Location, Coverage_Temporal_From, Coverage_Temporal_To, Coverage_Spatial_Type,
				Coverage_Spatial_Value, Existence_Start, Existence_End, Website, Data_Quality_Information, Reuse_Information, Access_Policy, URI, Description
				from mint_services ";

	   $result = mysql_query($sql);
	   echo mysql_error();
	   $rows = mysql_num_rows($result);
	    
	    $str_out = '"ID","Name","Type","ANZSRC_FOR_1","ANZSRC_FOR_2","ANZSRC_FOR_3","Location","Coverage_Temporal_From","Coverage_Temporal_To","Coverage_Spatial_Type","Coverage_Spatial_Value","Existence_Start","Existence_End","Website","Data_Quality_Information","Reuse_Information","Access_Policy","URI","Description"';  //variable to accumulate data to be written to csv file
	    $str_out .= "\n";        
	    echo("<table>");       
	    echo("<tr><th>ID<TH>Name<TH>Type<TH>ANZSRC_FOR_1<TH>ANZSRC_FOR_2<TH>ANZSRC_FOR_3<TH>Location<TH>Coverage_Temporal_From<TH>Coverage_Temporal_To<TH>Coverage_Spatial_Type<TH>Coverage_Spatial_Value<TH>Existence_Start<TH>Existence_End<TH>Website<TH>Data_Quality_Information<TH>Reuse_Information<TH>Access_Policy<TH>URI<TH>Description");
	    for($i=0; $i < $rows; $i++)
	    {
	     
		$record = mysql_fetch_row($result);
		
		echo("<tr><td>");        //ID
		echo $record[1];
		$str_out .= '"'.trim($record[1]).'",';
		
		echo("<td>");        //Name
		echo $record[2];
		$str_out .= '"'.trim($record[2]).'",';
		
		echo("<td>");   //type
		echo $record[3];
		$str_out .= '"'.trim($record[3]).'",';
		
		echo("<td>");        //FOR 1
		echo $record[4];     
		$str_out .= '"'.trim($record[4]).'",';	
		
		echo("<td>");        //FOR 2
		echo $record[5];   
		$str_out .= '"'.trim($record[5]).'",';
		
		echo("<td>");        //FOR 3
		echo $record[6];  
		$str_out .= '"'.trim($record[6]).'",';	
		
		echo("<td>");        //Location
		echo $record[7];  
		$str_out .= '"'.trim($record[7]).'",';	
		
		echo("<td>");        //Coverage_Temporal_From
		echo $record[8];  
		$str_out .= '"'.trim($record[8]).'",';	
		
		echo("<td>");        //Coverage_Temporal_To
		echo $record[9];  
		$str_out .= '"'.trim($record[9]).'",';	
		
		echo("<td>");        //Coverage_Spatial_Type
		echo $record[10];  
		$str_out .= '"'.trim($record[10]).'",';	
		
		echo("<td>");        //Coverage_Spatial_Value
		echo $record[11];  
		$str_out .= '"'.trim($record[11]).'",';	
		
		echo("<td>");        //Existence_Start
		echo $record[12];  
		$str_out .= '"'.trim($record[12]).'",';	
		
		echo("<td>");        //Existence_End
		echo $record[13];  
		$str_out .= '"'.trim($record[13]).'",';	
		
		echo("<td>");        //Website,
		echo $record[4];  
		$str_out .= '"'.trim($record[14]).'",';	
		
		echo("<td>");        //Data_Quality_Information,
		echo $record[15];  
		$str_out .= '"'.trim($record[15]).'",';	
		
		echo("<td>");        //Reuse_Information,
		echo $record[16];  
		$str_out .= '"'.trim($record[16]).'",';	
		
		echo("<td>");        //Access_Policy,
		echo $record[17];  
		$str_out .= '"'.trim($record[17]).'",';	      

		
		echo("<td>");        //URI
		echo $record[18];  
		$str_out .= '"'.trim($record[18]).'",';	 
		
		echo("<td>");        //Description
		
		$description = $record[19]; 
		
		$d = strtok($description, "\n\r");
		while($x =strtok( "\n\r"))
		{
			$d .= " ".$x;
		}
		echo $d;  
		$str_out .= '"'.trim($d).'"';	 
		
		$str_out .= "\n"; 
	    }
	    echo("</table>");   

	//echo $str_out;

	$outfile = fopen('Services_Swin.csv', "w");
		
		
	if(fwrite($outfile, $str_out))
	{
		echo "<br /><br />data written to file";
		$file_count++;
	}
	else
	{
		echo "<br /><br />write failed!";
		$errors = 1;
	}
	fclose($outfile);
	
	echo("<br /><br />+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br /><br />");
	if($errors)
	{
		echo("<h3>Warning errors detected - please fix and rerun</h3>");
	}
	else
	{
		echo("<h3>Do not attempt to transfer files before veryifying file contents and structure</h3>");
		
		echo("<br /><a href = \"Parties_Groups_Swin.csv\" >Parties_Groups_Swin.csv</a>");
		echo("<br /><a href = \"Parties_People_Swin.csv\" >Parties_People_Swin.csv</a>");
		echo("<br /><a href = \"Activities_Swin.csv\" >Activities_Swin.csv</a>");
		echo("<br /><a href = \"Services_Swin.csv\" >Services_Swin.csv</a>");

		echo("<FORM METHOD=\"post\" ACTION=\"mint_extract.php\">");   
		 echo("<input type = \"hidden\" name = \"transfer\" value = \"transfer\" />");

		
		echo("<INPUT TYPE=\"submit\" value=\"Transfer files to Redbox Server\" />");
		echo("</FORM>");

	}


}
else if($_POST['transfer'] == 'transfer')
{
	$output = array();
	
	$return = 1;

	$return = 1;

	exec("pscp.exe -pw swinres_ands Parties_Groups_Swin.csv redbox@redbox-vm2.cc.swin.edu.au:/opt/mint/home/data ", $output, $return);
	if($return == 0)
	{
		echo("<br /> Parties_Groups_Swin.csv sucessfully transferred to redbox-vm2.cc.swin.edu.au:/opt/mint/home/data");
	}
	else
	{
		echo("<br /> Parties_Groups_Swin.csv transfer failed!");
	}
	
	$return = 1;

	exec("pscp.exe -pw swinres_ands Parties_People_Swin.csv redbox@redbox-vm2.cc.swin.edu.au:/opt/mint/home/data ", $output, $return);
	if($return == 0)
	{
		echo("<br /> Parties_People_Swin.csv sucessfully transferred to redbox-vm2.cc.swin.edu.au:/opt/mint/home/data");
	}
	else
	{
		echo("<br /> Parties_People_Swin.csv transfer failed!");
	}
	
	$return = 1;

	exec("pscp.exe -pw swinres_ands Activities_Swin.csv redbox@redbox-vm2.cc.swin.edu.au:/opt/mint/home/data ", $output, $return);
	if($return == 0)
	{
		echo("<br /> Activities_Swin.csv sucessfully transferred to redbox-vm2.cc.swin.edu.au:/opt/mint/home/data");
	}
	else
	{
		echo("<br /> Activities_Swin.csv transfer failed!");
	}
	
	$return = 1;
	
	exec("pscp.exe -pw swinres_ands Services_Swin.csv redbox@redbox-vm2.cc.swin.edu.au:/opt/mint/home/data ", $output, $return);
	//exec("pscp.exe -pw swinres_ands test.csv redbox@redbox-vm2.cc.swin.edu.au:/opt/mint/home/data ", $output, $return);
	if($return == 0)
	{
		echo("<br /> Services_Swin.csv sucessfully transferred to redbox-vm2.cc.swin.edu.au:/opt/mint/home/data");
	}
	else
	{
		echo("<br /> Services_Swin.csv transfer failed!");
	}

}

?>